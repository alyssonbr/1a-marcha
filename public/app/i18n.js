angular.module('quaestoo.app.i18n', [])

.config(function($translateProvider) {
    var i18n = [];
    i18n['default'] = 'pt_BR';
    i18n['pt_BR'] = {
        'faltamCaracteres': 'restam {{count}} caracteres.',
        
        'msg.server.500': 'O servidor respondeu com um erro interno. Por favor, tente novamente mais tarde.',
        'msg.server.504': 'O servidor não pôde fazer a requisição. Tente novamente mais tarde.',
        
        'msg.arquivoInvalido': 'O arquivo escolhido não é permitido.',
        'msg.uploadErro': 'Ouve um erro ao enviar o arquivo. Tente novamente mais tarde.',
        'msg.user.senhaNaoConfere': "As duas senhas informadas não conferem.",
        'msg.login.usuarioSenha': 'O usuário e senha informados não conferem. Verifique e tente novamente.',
        'msg.criar.usuarioJaExiste': 'O email que você escolheu já está sendo utilizado por outro usuário. Escolha outro e tente novamente.',
        'msg.lembrar.emailEnviado': "Um email foi enviado para sua conta com a nova senha. Lembre-se de trocá-la após entrar no sistema.",
        'msg.salvar.ok': 'Seus dados foram atualizados com sucesso!',
        'msg.ansqr.salvar.ok': 'Parabéns! Seu AnsQR está salvo.',
        
        'home.menuTooltip': 'Página inicial',
        
        'profile.menuTooltip': 'Minha Conta',
        'profile.firstNameLbl': 'Primeiro Nome',
        'profile.lastNameLbl': 'Segundo Nome',
        'profile.emailLbl': 'Email',
        'profile.senhaLbl': 'Trocar Senha',
        'profile.verificarSenhaLbl': 'Verificar Senha',
        'profile.salvarBtn': 'Salvar',
        'profile.logoffBtn': 'Logoff',
        'profile.trocarFotoBtn': 'Trocar Foto',
        'profile.titulo': 'Meu Perfil  ',
        'profile.subTitulo': '',
        'profile.skillsLbl': 'Adicione novas habilidades',    
        'profile.skillsTip': '* use virgula para separar',
        
        'login.emailLbl': 'Email',
        'login.senhaLbl': 'Senha',
        'login.verificarSenhaLbl': 'Verificar Senha',
        'login.loginBtn': 'Login',
        'login.lembrarBtn': 'Lembrar Senha',
        'login.criarBtn': 'Criar Novo',
        'login.voltarBtn': 'Voltar',
        'login.titulo': 'Login',
        'login.criarTitulo': 'Criar Perfil',
        'login.lembrarTitulo': 'Lembrar Senha',
        'login.topBarBtn': 'Entrar',
        
        'maps.searchPlaceLbl': 'Busque seu local'
    };
    
    $translateProvider.translations('pt_BR', i18n[i18n['default'] || 'pt_BR']);

    $translateProvider.preferredLanguage('pt_BR');
});