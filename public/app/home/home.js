'use strict';

angular.module('quaestoo.home', [])

.config(function($stateProvider) {
    $stateProvider.state('app.home', {
        url: '/home',
        views: {
            'content': {
                templateUrl: 'home/home.html',
                controller: 'HomeCtrl'
            }
        }
    });
})

.controller('HomeCtrl', function($scope, UserService, AppService) {

    $scope.user = UserService.localUser();
    
    $scope.initHome = function() {
        
    };
});