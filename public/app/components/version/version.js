'use strict';

angular.module('quaestoo.version', [
  'quaestoo.version.interpolate-filter',
  'quaestoo.version.version-directive'
])

.value('version', '0.1');
