'use strict';

angular.module('pmApp.user', [])

.config(function($stateProvider) {
    $stateProvider.state('app.user', {
        url: '/user',
        views: {
            'content': {
                templateUrl: 'user/user.html',
                controller: 'UserCtrl'
            }
        }
    })
    .state('app.user.login', {
        views: {
            'userContent': {
                templateUrl: 'user/login.html'
            }
        }
    })
    .state('app.user.profile', {
        views: {
            'userContent': {
                templateUrl: 'user/profile.html'
            }
        }
    });
})

.controller('UserCtrl', function($scope, $state, UserService, AppConfig, AppService, SkillService) {
    
    // controls
    $scope.createView = false;
    $scope.rememberView = false;
    $scope.panelTitle = "Login";
    
    $scope.initPage = function() {
        $scope.user = UserService.localUser(); 
        
        if(! $scope.user._id) {
            $state.go('app.user.login');
        } else {
            $state.go('app.user.profile')
        }
    };
    
    $scope.initProfile = function() {
        var buttonId = 'photoUploadBtn',
            uploader;
                       
        $scope.user = UserService.localUser(); 
        
        $scope.map = { 
            center: { 
                latitude: $scope.user.location.position[0], 
                longitude: $scope.user.location.position[1] 
            }, 
            zoom: 15,
            options: {
                scrollwheel: false
            }
        };
        
        $scope.searchbox = { 
            template: 'searchbox.tpl.html', 
            events: {
                places_changed: function (searchBox) {
                    var lat, lng, address;
                    
                    if(searchBox.getPlaces() && searchBox.getPlaces().length > 0) {
                        
                        lat = searchBox.getPlaces()[0].geometry.location.lat();
                        lng = searchBox.getPlaces()[0].geometry.location.lng();
                        address = searchBox.getPlaces()[0].formatted_address;
                        
                        $scope.user.location.address = address;
                        $scope.user.location.position = [lat, lng];
                        
                        $scope.map.center = {
                            latitude: lat,
                            longitude: lng
                        };

                        angular.forEach($scope.markers, function(value, key) {
                            if(value.id === 1) {
                                $scope.markers[key].latitude = lat;
                                $scope.markers[key].longitude = lng;
                            }
                        });
                    }
                }
            }
        };
        
        $scope.markers = [{
            id: 1,
            latitude: $scope.user.location.position[0],
            longitude: $scope.user.location.position[1],
            icon: "/images/markers/repair-icon.png"
        }];
        
        uploader = new ss.SimpleUpload({
            button: buttonId,
            url: AppConfig.baseUrl + AppConfig.baseApiV1 + 'image/upload/user/' + $scope.user._id,
            responseType: 'json',
            name: 'image',                
            multipart: true,
            allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
            customHeaders: {
                'Authentication-Token': $scope.user.token
            },
            onExtError: function(filename, extension) {
                AppService.alertError('msg.');
            },
            onSubmit: function(filename, extension) {
                $('#' + buttonId).attr("disabled", "disabled");
            },         
            onComplete: function(filename, response) {
                $('#' + buttonId).attr("disabled", null);
                
                if (!response) {
                    return AppService.alertError('msg.uploadErro');   
                }

                $scope.user.photoUrl = response.path + response.fileName;
                $scope.user.picture = UserService.profileImage($scope.user);
                
                $scope.$apply($scope.user);

                AppService.set('user', $scope.user);
            }
        });
    };
    
    $scope.login = function() {
        UserService.login($scope.user.email, $scope.user.password);
    };
    
    $scope.logoff = function () {
        UserService.logoff();
    }
    
    $scope.create = function() {
        if($scope.user.password != $scope.user.password2) {
            return AppService.alertError('msg.user.senhaNaoConfere');
        }
        
        UserService.createProfile($scope.user.email, $scope.user.password);
    };
    
    $scope.save = function() {
        if(($scope.user.password.trim() && $scope.user.password.trim()) && $scope.user.password != $scope.user.password2) {
            return AppService.alertError('msg.user.senhaNaoConfere');
        }
        
        UserService.saveProfile($scope.user);
    };
    
    $scope.remember = function() {
        UserService.rememberPassword($scope.user.email);
    };
    
    $scope.toggleCreate = function() {
        AppService.alertClear();
        
        $scope.createView = !$scope.createView;       
    };
    
    $scope.toggleRemember = function() {
        AppService.alertClear();
        
        $scope.rememberView = !$scope.rememberView;
    };
    
    $scope.loadSkills = function(query) {
        return SkillService.fetchSkills(query);
    };
})

.factory('UserService', function($http, $state, $localStorage, AppConfig, AppService) {
    var profileImage,
        isSocialLogin,
        login,
        createProfile,
        rememberPassword,
        saveProfile,
        logoff,
        localUser,
        removeUser;

    profileImage = function (userModel) {
        if (userModel.photoUrl) {
            return AppConfig.baseUrl + userModel.photoUrl;
        } else {
            return AppConfig.baseUrl + 'images/default-avatar.png';
        }
    };

    isSocialLogin = function (userModel) {
        return userModel.social && (userModel.social.facebookId || userModel.social.googleId);
    };

    login = function (email, password) {
        var data = {
            "email": email,
            "password": password
        };

        $http.post(AppConfig.baseUrl + AppConfig.baseApiV1 + 'users/login', data)
            .success(
                function (user, status, header, config) {
                    user.picture = profileImage(user);
                    
                    localUser(user);

                    // configure connection headers
                    $http.defaults.headers.common['Authentication-Token'] = user.token;

                    $state.go('app.user.profile');
                }
            )
            .error(
                function (data, status, header, config) {
                    if (status === 403 || status === 404) {
                        AppService.alertError('msg.login.usuarioSenha');
                    }
                }
        );
    };

    createProfile = function (email, password) {
        var data = {
            "email": email,
            "password": password
        };

        $http.post(AppConfig.baseUrl + AppConfig.baseApiV1 + 'users', data)
            .success(
                function (user, status, header, config) {
                    localUser(user);

                    $http.defaults.headers.common['Authentication-Token'] = user.token;

                    $state.go('app.user.profile');
                }
            )
            .error(
                function (data, status, header, config) {
                    if (status === 409) {
                        AppService.alertError('msg.criar.emailJaExiste');
                    }
                }
        );
    };

    rememberPassword = function (email) {
        var data = {
            "email": email
        };

        $http.post(AppConfig.baseUrl + AppConfig.baseApiV1 + 'users/remember', data)
            .success(
                function (user, status, header, config) {
                    AppService.alertSuccess('msg.lembrar.emailEnviado');
                }
        );
    };

    saveProfile = function (user) {
        $http.put(AppConfig.baseUrl + AppConfig.baseApiV1 + 'users/' + user._id, user)
            .success(
                function (data, status, header, config) {
                    localUser(user);
                    
                    AppService.alertSuccess('msg.salvar.ok');
                }
            )
            .error(
                function (data, status, header, config) {
                    if (status === 409) {
                        AppService.alertError('msg.salvar.emailJaExiste');
                    }
                }
        );
    };
    
    localUser = function(user) {
        if(user) {
            $localStorage.setObject('user', user);
        } else {
            return $localStorage.getObject('user');
        }
    };
    
    removeUser = function(){
        $localStorage.del('user');
    };
    
    logoff = function () {
        removeUser();

        $state.go('home');
    };

    return {
        profileImage: profileImage,
        isSocialLogin: isSocialLogin,
        login: login,
        logoff: logoff,
        createProfile: createProfile,
        rememberPassword: rememberPassword,
        saveProfile: saveProfile,
        localUser: localUser,
        removeUser: removeUser
    };
}); 