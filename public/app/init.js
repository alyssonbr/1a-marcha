'use strict';

// Declare app level module which depends on views, and components
angular.module('pmApp', [
    'ui.router',
    'ui.bootstrap',
    'pascalprecht.translate',
    'angularMoment',
    'pageslide-directive',
    'ngTagsInput',
    'ngSanitize',
    'uiGmapgoogle-maps',
    'pmApp.app',
    'quaestoo.app.services',
    'quaestoo.app.directives',
    'quaestoo.app.i18n',
    'quaestoo.version',
    'quaestoo.home',
    'pmApp.user'
])

.run(function($state, $rootScope, AppService, amMoment) {
    $rootScope.previousState = {};
    
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
        // store previous state in $rootScope
        $rootScope.previousState.name = fromState.name;
        $rootScope.previousState.params = fromParams;
        
        $rootScope.alerts = [];
    });
    
    $rootScope.backState = function () {
        if ($rootScope.previousState.name !== "") {
            $state.go($rootScope.previousState.name, $rootScope.previousState.params);
        } else {
            $state.go('home');
        }
    };

    $rootScope.$on('http:start', function () {
        
    });

    $rootScope.$on('http:stop', function () {
        
    });
    
    $rootScope.$on('http:error', function (event, error) {
        if(error.response) {
            switch(error.response.status) {
                case 0:
                    AppService.alertError('Não ouve resposta do servidor. Por favor, tente novamente mais tarde.');
                    break;
                case 401:
                    $state.go('user.login');
                    
                    break;
                case 500:
                    console.log(error);
                    AppService.alertError('msg.server.500');
                    
                    break;
                case 504:
                    AppService.alertError('msg.server.504');
                    
                    break;
            }
        }
    });
    
    // alert handling
    $rootScope.alerts = [];
    $rootScope.alert = function(message, toAdd, type) {
        if(! toAdd) {
            $rootScope.alerts = [];
        }
        $rootScope.alerts.push({type: type, msg: message});
    };
    $rootScope.closeAlert = function(index) {
        $rootScope.alerts.splice(index, 1);
    };
    
    // momentjs
    amMoment.changeLocale('pt-br');
})

.config(function($urlRouterProvider, $httpProvider, uiGmapGoogleMapApiProvider) {
    
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/home');

    // register error handler for http calls
    $httpProvider.interceptors.push('HttpHandlers');
    
    uiGmapGoogleMapApiProvider.configure({
        //    key: 'your api key',
        v: '3.17',
        libraries: 'places'
    });
});