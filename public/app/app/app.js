'use strict';

angular.module('pmApp.app', [])

.config(function($stateProvider) {
    $stateProvider.state('app', {
        views: {
            'home': {
                templateUrl: 'app/app.html',
                controller: 'AppCtrl'
            }
        }
    });
})

.controller('AppCtrl', function($scope, $state, $modal, $location, $timeout, UserService, AppService) {
    $scope.user = UserService.localUser();
    
    $scope.initApp = function() {        
        $scope.user.picture = UserService.profileImage($scope.user);
        
        $scope.checkMenu();
    };
    
    $scope.checkMenu = function() {
        $scope.showMenu = $location.path().indexOf('/qr') === -1;
    };
    
    $scope.showCompleteMenu = function() {        
        $timeout.cancel($scope.hideTimeoutPromise);
        
        $scope.showTimeoutPromise = $timeout(function() {
            $('.pgMenu').animate({width: '180px'}, {
                duration: 300,
                complete: function() {
                    $(this).css('box-shadow', '0 0 5px #000');

                    $scope.showMenuText = true;
                    $scope.$apply();
                }
            });

            /*$('.logoContainer').animate({width: '170px'}, {
                duration: 300,
                complete: function() {
                    $('.brand').fadeIn('slow');
                }
            });*/
        }, 1000);
    };
    
    $scope.hideCompleteMenu = function() {
        $timeout.cancel($scope.showTimeoutPromise);
            
        $scope.hideTimeoutPromise = $timeout(function() {
            
            //$('.brand').fadeOut('fast', function() {
                $scope.showMenuText = false;
                $scope.$apply();
            
                $('.pgMenu').animate({width: '75px'}, {
                    duration: 300,
                    done: function() {
                        $(this).css('box-shadow', '');
                    }
                });
                
                /*$('.logoContainer').animate({width: '62px'}, {
                    duration: 300
                });*/
            //});            
        }, 1000);
    }
    
    $scope.isLogged = function () {
        return UserService.localUser() !== undefined && UserService.localUser()._id !== undefined;
    };

    $scope.logout = function () {
        UserService.removeUser();

        $state.go('app.home');
    };
})

.factory('AppService', function (AppConfig, $localStorage, $http, $state, $rootScope, $translate) {
    var conf = {},
        set,
        get,
        del,
        alertClear,
        alertSuccess,
        alertError,
        i18n,
        back;


    set = function (key, value) {
        var hasVehicle = false;

        conf[key] = value;
    };

    get = function (key, id) {
        var value,
            vehicle;

        return conf[key];
    };

    del = function (key) {
        delete conf[key];
    };
    
    alertSuccess = function(msg, toAdd) {
        $translate(msg).then(function(value) {
            //$rootScope.alert(value, toAdd, "success");
            noty({text: value, type: 'success', position: 'top', 
                  theme: 'relax',
                  timeout: 4000,
                  animation: {                      
                      open: 'animated bounceInDown',                      
                      close: 'animated flipOutX',
                      speed: 500
                  }});
        });        
    };
    
    alertError = function(msg, toAdd) {
        $translate(msg).then(function(value) {
            //$rootScope.alert(value, toAdd, "danger");
            noty({text: value, type: 'error', position: 'top',
                  theme: 'relax',
                  timeout: 4000,
                  animate: {
                      theme: 'relax',
                      open: 'animated bounceInDown',
                      close: 'animated flipOutX',
                      speed: 500
                  }});
        });
    };
    
    alertClear = function() {
        $rootScope.alerts = [];
        $.noty.closeAll();
    };
    
    back = function() {
        $rootScope.backState();
    };

    return {
        set: set,
        get: get,
        del: del,
        alertClear: alertClear,
        alertSuccess: alertSuccess,
        alertError: alertError,
        i18n: i18n,
        back: back
    };
});