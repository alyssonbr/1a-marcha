angular.module('quaestoo.app.directives', [])

.directive('mdEditor', function() {
    return {
        restrict: 'A',
        scope: {
            model: '=ngModel'
        },
        link: function(scope, elm, attrs) {
            elm.markdown({
                autofocus: true,
                height: 300,
                hiddenButtons: ['cmdPreview', 'cmdImage'],
                resize: 'vertical'
            });
        }
    };
})

.directive('questionField', function() {
    return {
        restrict: 'A',
        scope: {},
        link: function(scope, elm, attrs) {
            var sufix = "?";
            
            function setCaretPosition() {
                var pos;
                
                if(elm.val().length > 1) {
                    pos = elm.val().length - sufix.length;

                    elm[0].setSelectionRange(pos, pos);
                }
            }
            
            elm.on("keyup", function(event) {
                if(elm.val().length == 0 || elm.val().trim() == sufix) {
                    elm.val('');
                } else {
                    elm.val(elm.val().replace(/\?$/g, '') + sufix);
                    setCaretPosition();
                }
            });
            
            elm.on("focus", function(event) {
                setCaretPosition();
            });
            
            elm.on("click", function(event) {
                setCaretPosition();
            });
        }        
    }
})

.directive('inputTip', function() {
    return {
        restrict: 'E',
        scope: {
            content: '='
        },
        template: '<small class="inputTip critical">{{content | translate}}</small>'
    }
})

.directive('inputCountTip', function() {
    return {
        restrict: 'E',
        scope: {
            model: '=',
            maxlength: '=',
            critial: '=?'
        },
        template: '<small class="inputTip" ng-class="{\'critical\': maxlength - model.length <= critical}">{{\'faltamCaracteres\' | translate:\'{count:maxlength - model.length}\'}}</small>',
        controller: function($scope) {
            $scope.critical = $scope.critical || 5;
        }
    }
})

.directive('loading', function ($http) {
    return {
        restrict: 'A',
        scope: {},
        link: function (scope, elm, attrs)
        {
            var loader,
                icon,
                subist = false,
                loaderClass = "fa fa-spin fa-spinner",
                iconClass;
            
            icon = elm.find("i.fa");
            
            if(icon.length > 0) {
                loader = icon[0];
                iconClass = $(loader).attr("class"); 
                subist = true;
            } else {            
                loader = $("<i>").addClass(loaderClass);
                elm.prepend(loader);
            }
            
            scope.isLoading = function() {
                return $http.pendingRequests.length > 0;
            };
            
            scope.$watch(scope.isLoading, function(loading) {
                if(! loading) {
                    if(subist) {
                        $(loader).removeClass(loaderClass).addClass(iconClass);
                    } else {
                        $(loader).hide();
                    }
                    
                    elm.attr("disabled", null);
                }
            });

            elm.on("click", function(event) {    
                var loading = scope.isLoading();
                
                if(loading) {
                    if(subist) {
                        $(loader).removeClass(iconClass).addClass(loaderClass);
                    } else {
                        $(loader).show();
                    }
                    
                    elm.attr("disabled", "disabled");
                }
            });
        }
    };
});