var requireDir = require('require-dir'),
    express = require('express'),
    app = express(),
    apiVersion = 'v1',
    services = requireDir('./app/services/' + apiVersion, {recurse: true}),
    vehiclePhotoPath = __dirname + '/public/app/vehicle-images/',
    userPhotoPath = __dirname + '/public/app/images/profiles',
    corsMiddleware;

corsMiddleware = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', 'http://localhost:3000');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authentication-Token');

    next();
}

app.use(corsMiddleware);
app.use(express.static(__dirname + '/public/app'));
app.use(express.bodyParser());

// define globals
global.vehiclePhotoPath = vehiclePhotoPath;
global.userPhotoPath = userPhotoPath;

function registerServices(app, services) {	
	var module;

	for(service in services) {
		module = services[service];

		if(typeof module === 'function') {
			module(app).init();
		} else if(typeof module === 'object') {
			registerServices(app, module);
		}	
	}
}

// register services
registerServices(app, services);

app.listen(3000);
console.log('Started listening at 3000');