var _ = require('underscore'),
    userService = require('../../../app/services/v1/user_service'),
    db = require('../../../app/db/db'),
    dbHelper = require('../../helpers/db-helper')(db);

describe('User Service', function() {
    var fakeExpress = {
            login: null,
            loginFacebook: null,
            loginGoogle: null,
            getUsers: null,
            createUser: null,
            updateUser: null,
            rememberPassword: null,
            get: function(path, callback) {
                if(callback.name == 'getUsers') {
                    this.getUsers = callback
                }
            },
            post: function(path, callback) {
                if(callback.name == 'login') {
                    this.login = callback;
                } else if(callback.name == 'loginFacebook') {
                    this.loginFacebook = callback;
                } else if(callback.name == 'loginGoogle') {
                    this.loginGoogle = callback;
                } else if(callback.name == 'createUser') {
                    this.createUser = callback;
                } else if(callback.name == 'rememberPassword') {
                    this.rememberPassword = callback;
                }
            },
            put: function(path, checkUser, callback) {
                if(callback.name == 'updateUser') {
                    this.updateUser = callback;
                }
            }
        },
        fakeRequest = {
            body: {},
            query: {}
        },
        fakeResponse = {
            status: 0,
            body: '',
            send: function(status, body) {
                if(body === undefined)
                {
                    this.status = 200;
                    this.body = status;
                } else {
                    this.status = status;
                    this.body = body;
                }
            }
        },
        responseSpy = null;

    beforeEach(function() {
        fakeRequest = {
            body: {}
        };
        fakeResponse.status = 0;
        fakeResponse.body = '';

        responseSpy = spyOn(fakeResponse, 'send').andCallThrough();

        db.init();
    });

    afterEach(function(done) {
        dbHelper.cleanDatabase(done);
    });

    it('Login should require data to run.', function() {
        userService(fakeExpress).init();
        fakeExpress.login(fakeRequest, fakeResponse);

        expect(fakeResponse.status).toBe(400);
    });

    it('Login should require a exiting user', function() {
        userService(fakeExpress).init();
        fakeRequest.body = {
            email: 'notadded@aisoft.com',
            password: 'whatever'
        };

        runs(function() {
            fakeExpress.login(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 0;
        });

        runs(function() {
            expect(fakeResponse.status).toBe(404);
        });
    });

    it('Login should require a valid local profiled user', function(done) {
        var user = db.userModel({
            firstName: 'Arlindo',
            password: 'does not matters',
            email: 'arlindo@aisoft.com.br',
            social: {
                facebookId: 'adsfadsf', // for facebook authenticated user
                googleId: 'asdfadsfd' // for google authenticated user
            }
        });

        var fail = false;
        runs(function() {
            user.save(function(err, user) {

                if(err) {
                    console.log(err);
                    fail = true;
                } else {

                    userService(fakeExpress).init();
                    fakeRequest.body = {
                        email: user.email,
                        password: user.password
                    };

                    fakeExpress.login(fakeRequest, fakeResponse);
                }
            });
        });

        waitsFor(function() {
            return responseSpy.callCount > 0 || fail;
        },'', 2000);

        runs(function() {
            expect(fakeResponse.status).toBe(404);

            dbHelper.cleanDatabase(done);
        });
    });

    it('Login should require a valid password', function(done) {
        var password = 'password';

        var user = db.userModel({
            firstName: 'Arlindo',
            password: db.userModel.createPassword(password),
            email: 'arlindo@aisoft.com.br'
        });

        var fail = false;
        runs(function() {
            user.save(function(err, user) {

                if(err) {
                    console.log(err);
                    fail = true;
                } else {

                    userService(fakeExpress).init();
                    fakeRequest.body = {
                        email: 'arlindo@aisoft.com.br',
                        password: 'whatever'
                    };

                    fakeExpress.login(fakeRequest, fakeResponse);
                }
            });
        });

        waitsFor(function() {
            return responseSpy.callCount > 0 || fail;
        },'', 2000);

        runs(function() {
            expect(fakeResponse.status).toBe(401);

            dbHelper.cleanDatabase(done);
        });
    });

    it('Login result should not return the password', function(done) {
        var password = 'password';

        var user = db.userModel({
            firstName: 'Arlindo',
            password: db.userModel.createPassword(password),
            email: 'arlindo@aisoft.com.br'
        });

        var fail = false;
        runs(function() {
            user.save(function(err, user) {

                if(err) {
                    console.log(err);
                    fail = true;
                } else {

                    userService(fakeExpress).init();
                    fakeRequest.body = {
                        email: 'arlindo@aisoft.com.br',
                        password: password
                    };

                    fakeExpress.login(fakeRequest, fakeResponse);
                }
            });
        });

        waitsFor(function() {
            return responseSpy.callCount > 0 || fail;
        },'', 2000);

        runs(function() {
            expect(fakeResponse.status).toBe(200);
            expect(fakeResponse.body.password).toBe('');

            dbHelper.cleanDatabase(done);
        });
    });

    it('Login result should return the user token', function(done) {
        var password = 'password';

        var user = db.userModel({
            firstName: 'Arlindo',
            password: db.userModel.createPassword(password),
            email: 'arlindo@aisoft.com.br'
        });

        var fail = false;
        runs(function() {
            user.save(function(err, user) {

                if(err) {
                    console.log(err);
                    fail = true;
                } else {

                    userService(fakeExpress).init();
                    fakeRequest.body = {
                        email: 'arlindo@aisoft.com.br',
                        password: password
                    };

                    fakeExpress.login(fakeRequest, fakeResponse);
                }
            });
        });

        waitsFor(function() {
            return responseSpy.callCount > 0 || fail;
        },'', 2000);

        runs(function() {
            expect(fakeResponse.status).toBe(200);
            expect(fakeResponse.body.token).toBeDefined();

            dbHelper.cleanDatabase(done);
        });
    });

    it('Create user should require data to run.', function() {
        userService(fakeExpress).init();
        fakeExpress.login(fakeRequest, fakeResponse);

        expect(fakeResponse.status).toBe(400);
    });

    it('Create user should not return the password', function(done) {
        runs(function() {
            userService(fakeExpress).init();
            fakeRequest.body = {
                password: 'password',
                email: 'alindo@aisoft.com.br'
            };

            fakeExpress.createUser(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 0;
        },'', 2000);

        runs(function() {
            expect(fakeResponse.status).toBe(201);
            expect(fakeResponse.body.password).toBe('');

            dbHelper.cleanDatabase(done);
        });
    });

    it('Create user should return a inactive user.', function(done) {
        runs(function() {
            userService(fakeExpress).init();
            fakeRequest.body = {
                password: 'password',
                email: 'alindo@aisoft.com.br'
            };

            fakeExpress.createUser(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 0;
        },'', 2000);

        runs(function() {
            expect(fakeResponse.status).toBe(201);
            expect(fakeResponse.body.active).toBe(false);

            dbHelper.cleanDatabase(done);
        });
    });

    it('Create user should return a user with non-admin profile.', function(done) {
        runs(function() {
            userService(fakeExpress).init();
            fakeRequest.body = {
                password: 'password',
                email: 'alindo@aisoft.com.br'
            };

            fakeExpress.createUser(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 0;
        },'', 2000);

        runs(function() {
            expect(fakeResponse.status).toBe(201);
            expect(fakeResponse.body.profile).toBe(3);

            dbHelper.cleanDatabase(done);
        });
    });

    it('Create user should return a fetch token for the user.', function(done) {
        runs(function() {
            userService(fakeExpress).init();
            fakeRequest.body = {
                password: 'password',
                email: 'alindo@aisoft.com.br'
            };

            fakeExpress.createUser(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 0;
        },'', 2000);

        runs(function() {
            expect(fakeResponse.status).toBe(201);
            expect(fakeResponse.body.fetchToken).not.toBe('');

            dbHelper.cleanDatabase(done);
        });
    });

    it('Create user should not be accomplished with an email that already exists.', function(done) {
        var password = 'password';

        var user = db.userModel({
            firstName: 'Arlindo',
            password: db.userModel.createPassword(password),
            email: 'arlindo@aisoft.com.br'
        });

        var fail = false;
        runs(function() {
            user.save(function(err, user) {

                if(err) {
                    console.log(err);
                    fail = true;
                } else {

                    userService(fakeExpress).init();
                    fakeRequest.body = {
                        password: 'password',
                        email: 'arlindo@aisoft.com.br'
                    };

                    fakeExpress.createUser(fakeRequest, fakeResponse);
                }
            });
        });

        waitsFor(function() {
            return responseSpy.callCount > 0 || fail;
        },'', 2000);

        runs(function() {
            expect(fakeResponse.status).toBe(409);

            dbHelper.cleanDatabase(done);
        });
    });

    it('Query should find users by first name', function(done) {
        runs(function() {
            userService(fakeExpress).init();
            fakeRequest.body = {
                firstName: 'Arlindo',
                lastName: 'Neto',
                password: 'password',
                email: 'alindo@aisoft.com.br'
            };

            fakeExpress.createUser(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 0;
        },'', 2000);

        runs(function() {
            fakeRequest.query = {
                query: 'lindo'
            };

            fakeExpress.getUsers(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 1;
        },'', 2000);

        runs(function() {
            expect(fakeResponse.status).toBe(200);
            expect(fakeResponse.body.email).not.toBe('alindo@aisoft.com.br');

            dbHelper.cleanDatabase(done);
        });
    });

    it('Query should find users by last name', function(done) {
        runs(function() {
            userService(fakeExpress).init();
            fakeRequest.body = {
                firstName: 'Arlindo',
                lastName: 'Neto',
                password: 'password',
                email: 'alindo@aisoft.com.br'
            };

            fakeExpress.createUser(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 0;
        },'', 2000);

        runs(function() {
            fakeRequest.query = {
                query: 'eto'
            };

            fakeExpress.getUsers(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 1;
        },'', 2000);

        runs(function() {
            expect(fakeResponse.status).toBe(200);
            expect(fakeResponse.body.email).not.toBe('alindo@aisoft.com.br');

            dbHelper.cleanDatabase(done);
        });
    });

    it('Query should find users by email', function(done) {
        runs(function() {
            userService(fakeExpress).init();
            fakeRequest.body = {
                firstName: 'Arlindo',
                lastName: 'Neto',
                password: 'password',
                email: 'alindo@aisoft.com.br'
            };

            fakeExpress.createUser(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 0;
        },'', 2000);

        runs(function() {
            fakeRequest.query = {
                query: 'aisoft'
            };

            fakeExpress.getUsers(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 1;
        },'', 2000);

        runs(function() {
            expect(fakeResponse.status).toBe(200);
            expect(fakeResponse.body.email).not.toBe('alindo@aisoft.com.br');

            dbHelper.cleanDatabase(done);
        });
    });

    it('Update user should not return the password', function(done) {
        var userId;

        userService(fakeExpress).init();

        runs(function() {
            fakeRequest.body = {
                password: 'password',
                email: 'alindo@aisoft.com.br'
            };

            fakeExpress.createUser(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 0;
        },'', 2000);

        runs(function() {
            userId = fakeResponse.body._id;

            fakeRequest.body = {
                firstName: "Arlindo"
            };
            fakeRequest.params = {
                id: userId
            };

            fakeExpress.updateUser(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 1;
        },'', 2000);

        runs(function() {
            expect(fakeResponse.status).toBe(200);
            expect(fakeResponse.body.password).toBe('');

            dbHelper.cleanDatabase(done);
        });
    });

    it('Update user password should not change the token', function(done) {
        var userId,
            userToken;

        userService(fakeExpress).init();

        runs(function() {
            fakeRequest.body = {
                password: 'password',
                email: 'alindo@aisoft.com.br'
            };

            fakeExpress.createUser(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 0;
        },'', 2000);

        runs(function() {
            userId = fakeResponse.body._id;
            userToken = fakeResponse.body.token;

            fakeRequest.body = {
                password: "pass"
            };
            fakeRequest.params = {
                id: userId
            };

            fakeExpress.updateUser(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 1;
        },'', 2000);

        runs(function() {
            expect(fakeResponse.status).toBe(200);
            expect(fakeResponse.body.token).toBe(userToken);

            dbHelper.cleanDatabase(done);
        });
    });

    it('Facebook login should require data to run.', function() {
        userService(fakeExpress).init();
        fakeExpress.loginFacebook(fakeRequest, fakeResponse);

        expect(fakeResponse.status).toBe(400);
    });

    it('Facebook login should create a new user on the first login.', function() {
        userService(fakeExpress).init();

        fakeRequest.body = {
            id: 'fakeFbId',
            first_name: 'Arlindo',
            last_name: 'Neto',
            email: 'fake@email.com'
        };

        runs(function() {
            fakeExpress.loginFacebook(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 0;
        });

        runs(function() {
            expect(fakeResponse.status).toBe(200);
            expect(fakeResponse.body.firstName).toBe('Arlindo');
            expect(fakeResponse.body.lastName).toBe('Neto');
            expect(fakeResponse.body.social.facebookId).toBe('fakeFbId');
            expect(fakeResponse.body.email).toBe('fake@email.com');
        });
    });

    it('Facebook login should create a users fetch token.', function() {
        userService(fakeExpress).init();

        fakeRequest.body = {
            id: 'fakeFbId',
            first_name: 'Arlindo',
            last_name: 'Neto',
            email: 'fake@email.com'
        };

        runs(function() {
            fakeExpress.loginFacebook(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 0;
        });

        runs(function() {
            expect(fakeResponse.status).toBe(200);
            expect(fakeResponse.body.fetchToken).not.toBe('');
        });
    });

    it('Google login should require data to run.', function() {
        userService(fakeExpress).init();
        fakeExpress.loginGoogle(fakeRequest, fakeResponse);

        expect(fakeResponse.status).toBe(400);
    });

    it('Google login should create a new user on the first login.', function() {
        userService(fakeExpress).init();

        fakeRequest.body = {
            id: 'googleId',
            first_name: 'Arlindo',
            last_name: 'Neto',
            email: 'fake@email.com',
            photo: 'http://www.whatever.it.is/photo.png'
        };

        runs(function() {
            fakeExpress.loginGoogle(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 0;
        });

        runs(function() {
            expect(fakeResponse.status).toBe(200);
            expect(fakeResponse.body.firstName).toBe('Arlindo');
            expect(fakeResponse.body.lastName).toBe('Neto');
            expect(fakeResponse.body.social.googleId).toBe('googleId');
            expect(fakeResponse.body.email).toBe('fake@email.com');
        });
    });

    it('Google login should create a users fetch token.', function() {
        userService(fakeExpress).init();

        fakeRequest.body = {
            id: 'fakeFbId',
            first_name: 'Arlindo',
            last_name: 'Neto',
            email: 'fake@email.com',
            photo: 'http://www.whatever.it.is/photo.png'
        };

        runs(function() {
            fakeExpress.loginFacebook(fakeRequest, fakeResponse);
        });

        waitsFor(function() {
            return responseSpy.callCount > 0;
        });

        runs(function() {
            expect(fakeResponse.status).toBe(200);
            expect(fakeResponse.body.fetchToken).not.toBe('');
        });
    });
    
    it('Remember password should send an email to the user', function(done) {
        var password = 'password',
            fail = false,
            user = db.userModel({
                firstName: 'Arlindo',
                password: db.userModel.createPassword(password),
                email: 'arlindosilvaneto@aisoft.com.br'
            });

        runs(function() {
            user.save(function(err, user) {

                if(err) {
                    console.log(err);

                    fail = true;
                } else {

                    userService(fakeExpress).init();
                    fakeRequest.body = {
                        email: 'arlindosilvaneto@aisoft.com.br'
                    };

                    fakeExpress.rememberPassword(fakeRequest, fakeResponse);
                }
            });
        });

        waitsFor(function() {
            return responseSpy.callCount > 0 || fail;
        },'', 5000);

        runs(function() {
            expect(fakeResponse.status).toBe(200);

            dbHelper.cleanDatabase(done);
        });
    });
});
