var async = require('async');
var _ = require('underscore');

module.exports = function(db) {

    var cleanDatabase = function(done) {
        var collections = _.keys(db.getDb().connection.collections);
        async.forEach(collections, function(collectionName, forDone) {
            var collection = db.getDb().connection.collections[collectionName];
            collection.drop(function(err) {
                if (err && err.message != 'ns not found') done(err);
                forDone(null);
            })
        }, function() {
            done();
        });
    };

    return {
        cleanDatabase: cleanDatabase
    };
};
