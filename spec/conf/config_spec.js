var conf = require('../../app/conf/config');

describe('Configuracoes', function() {
	
	it('should have a db url defined for tests', function() {
		expect(conf.test.db.url).toBeDefined();
	});

    it('should have a db url defined for development', function() {
        expect(conf.development.db.url).toBeDefined();
    });
});
