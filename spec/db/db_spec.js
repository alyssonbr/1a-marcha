var db = require('../../app/db/db');
var helper = require('../helpers/db-helper')(db);

describe('Database services', function() {

    beforeEach(function() {
        db.init();
    });

    it('should connect to database', function() {
        expect(db.getDb().connection.db).toBeDefined();
    });
});
