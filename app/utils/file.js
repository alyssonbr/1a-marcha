var fs = require('fs'),
    request = require('request'),
    crypto = require('crypto'),
    mkdirp = require('mkdirp'),
    Promise = require('mpromise'),
    publicPath = 'public/app',
    profileImagesPath = '/images/profiles/',
    env = process.env.NODE_ENV;

exports.saveProfileImage = function(url, baseFileName) {
    var fileName,
        promise = new Promise();

    request.head({
        url: url,
        followAllRedirects: true
    }, function(err, res){
        fileName = profileImagesPath + baseFileName + '.' + res.headers['content-type'].split('/')[1];
		
        if(fileName.indexOf(' ') === -1) {
            request({
                url: url,
                followAllRedirects: true
            }).pipe(fs.createWriteStream(publicPath + fileName)).on('close', function() {
                console.log('Image File: ' + fileName);
                promise.fulfill(fileName);
            });
        } else {
            promise.fulfill('[fake fileName]');
        }
    });

    return promise;
};

exports.savePublicFile = function(path, whatFor, callback, suggestName) {        
    var ext = path.substring(path.lastIndexOf(".")),
        fileId = suggestName || createFileName(path),
        fileName = fileId + ext,
        targetPath,
        targetFile;
    
    if(whatFor == 'vehicle') {
        targetPath = global.vehiclePhotoPath;        
    } else if(whatFor == 'user') {
        targetPath = global.userPhotoPath;
    }
    
    targetFile = targetPath + '/' + fileName;
    
    mkdirp(targetPath, function(err) {
        fs.readFile(path, function (err, data) { 
            if(err) {
                callback(err);
                return;
            }
            
            fs.writeFile(targetFile, data, function (err) {
                if(err) {
                    callback(err);
                    return;
                }
                
                fs.unlink(path, function(err) {
                    if(err) {
                        callback(err);
                        return;
                    }
                    
                    callback(null, fileId + ext, targetPath);
                });
            });
        });
    });
};

exports.savePublicFileFromUrl = function(url, callback) {        
    var ext = url.substring(url.lastIndexOf(".")),
        fileId = createFileName(url),
        fileName = fileId + ext,
        targetPath = 'public/app/vehicle-images',
        targetFile = targetPath + '/' + fileName;
    
    mkdirp(targetPath, function(err) {
        request(url).pipe(fs.createWriteStream(targetFile));
        
        callback(null, fileName);
    });
};

function createFileName(path) {
    var sha1 = crypto.createHash('sha1');

    sha1.update(path);

    return sha1.digest('hex');
};