var nodemailer = require('nodemailer'),
    Promise = require('mpromise'),
    env = process.env.NODE_ENV || 'development',
    config = require('../conf/config')[env];

exports.sendEmail = function(to, subject, content) {
    var promise = new Promise(),
        transporter,
        mailOptions;

    // create reusable transporter object using SMTP transport
    transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: config.email.accountUser,
            pass: config.email.accountPassword
        }
    });

    mailOptions = {
        from: config.email.sender, // sender address
        to: to, // list of receivers
        subject: subject, // Subject line
        text: content
    };

    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            promise.reject(error)
        }else{
            promise.fulfill(info);
        }
    });

    return promise;
};