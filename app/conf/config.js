exports.development = {
    db: {
       url: "mongodb://1marcha:1marcha_123@dogen.mongohq.com:10043/primeira-marcha-dev"
    },
    baseApi: '/api/v1',
    passwordChangeUrl: '[fakeUrl]',
    email: {        
        accountUser: 'arlindosilvaneto@aisoft.com.br',
        accountPassword: 'Admin(123)',
        sender: 'AISoft <arlindosilvaneto@aisoft.com.br>'
    }
};

exports.test = {
    db: {
       //url: "mongodb://aisoft:super123@kahana.mongohq.com:10079/aisoftapi_test"
       url: 'mongodb://localhost:27017/aisoftapi_test'
    },
    baseApi: '/api/v1',
    passwordChangeUrl: '[fakeUrl]',
    email: {        
        accountUser: 'arlindosilvaneto@aisoft.com.br',
        accountPassword: 'Admin(123)',
        sender: 'AISoft <arlindosilvaneto@aisoft.com.br>'
    }
};

exports.production = {
    db: {
        url: "mongodb://arlindosilvaneto:super123@dogen.mongohq.com:10073/quaestoo-development"
    },
    baseApi: '/api/v1',
    passwordChangeUrl: '[fakeUrl]',
    email: {        
        accountUser: 'arlindosilvaneto@aisoft.com.br',
        accountPassword: 'Admin(123)',
        sender: 'AISoft <arlindosilvaneto@aisoft.com.br>'
    }
};
