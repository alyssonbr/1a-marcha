exports.extractTags = function(text) {
    var tagList = [],
        tagRegex = /[\s]*#([a-zA-Z0-9]*)[\s]*/g,
        matches,
        index;
    
    while(matches = tagRegex.exec(text)) {
        tagList.push(matches[1]);
    }

    return tagList;
};