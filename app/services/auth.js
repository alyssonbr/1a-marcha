/**
 * Description: Authentication and authorization service
 * Author: Arlindo Neto
 * Created at: 2014-01-18
 */

var _ = require('underscore'),
    db = require('../db/db'),
    async = require('async'),
    env = process.env.NODE_ENV || 'development',
    config = require('../conf/config')[env],
    authenticationToken = 'authentication-token';

module.exports = function() {

    var checkUser = function(req, res, next) {
        if(! req.user) {
            var token = req.headers[authenticationToken];

            db.init();
            var user = db.userModel;

            var promise = user.findOne({"token": token}).exec();

            promise.onReject(function(error) {
                res.send(500, error);
            });

            promise.onFulfill(function(model) {
                if(model && model.profile <= 3) {
                    req.user = model;

                    next();
                } else {
                    res.send(401, 'Authentication needed');
                }
            });
        } else {
            next();
        }
    };

    var checkOptionalUser = function(req, res, next) {
        if(! req.user) {
            var token = req.headers[authenticationToken];

            db.init();

            var promise = db.userModel.findOne({"token": token}).exec();

            promise.onReject(function(error) {
                res.send(500, error);
            });

            promise.onFulfill(function(model) {
                if(model) {
                    req.user = model;
                }

                next();
            });
        } else {
            next();
        }
    };

    var checkAdmin = function(req, res, next) {
        checkUser(req, res, function() {
            var user = req.user;
            
            if(!user) {
                res.send(401, 'Authentication needed');
            } else if(user.profile === 1) {
                next();
            } else {
                res.send(403, 'Not Authorized');
            }
        });
    };
    
    var checkOperator = function(req, res, next) {
        checkUser(req, res, function() {
            var user = req.user;
            
            if(!user) {
                res.send(401, 'Authentication needed');
            } else if(user.profile <= 2) {
                next();
            } else {
                res.send(403, 'Not Authorized');
            }
        });
    };

    /*
     * Need to be called before checkUser to allow applications
     * to fetch special data without being logged
     */
    var checkApp = function(req, res, next) {
        var fetchToken = req.query['fetch-token'];

        db.init();
        var user = db.userModel;

        var promise = user.findOne({"fetchToken": fetchToken}).exec();

        promise.onReject(function(error) {
            res.send(500, error);
        });

        promise.onFulfill(function(model) {
            if(model) {
                if(model.profile && model.profile == 1) {
                    req.user = model;

                    next();
                } else {
                    res.send(401, 'Authentication needed');
                }
            } else {
                res.send(401, 'Authentication needed');
            }
        });
    };

    return {
        checkUser: checkUser,
        checkOptionalUser: checkOptionalUser,
        checkOperator: checkOperator,
        checkAdmin: checkAdmin,
        checkApp: checkApp
    };
};