/**
 * Description: Question Services
 * Author: Arlindo Neto
 * Created at: 2015-01-16
 */

var _ = require('underscore'),
    mongoose = require('mongoose'),
    db = require('../../db/db'),
    env = process.env.NODE_ENV || 'development',
    config = require('../../conf/config')[env],
    auth = require('../auth'),    
    checkRegularUser = new auth().checkUser;

module.exports = function(express) {    
    var colPath = config.baseApi + '/skills',
        init,
        fetchSkills;
    
    init = function() {
        express.get(colPath + '/:query', fetchSkills);
    };
    
    fetchSkills = function fetchSkills(req, res) {
        var query = req.params.query,
            promise;
        
        db.init();
        
        promise = db.skillModel.find({description: new RegExp(".*" + query + ".*", 'i')}).exec();
        
        promise.onReject(function(err) {
            res.send(500, err);
        });
        
        promise.onFulfill(function(skills) {
            var results = [];
            
            skills.forEach(function(skill) {
                results.push({"text": skill.description});
            });
            
            res.send(200, results);
        });
    };    
    
    return {
        init: init
    };
};