/**
 * Description: Question Services
 * Author: Arlindo Neto
 * Created at: 2015-01-16
 */

var _ = require('underscore'),
    mongoose = require('mongoose'),
    db = require('../../db/db'),
    env = process.env.NODE_ENV || 'development',
    config = require('../../conf/config')[env],
    auth = require('../auth'),    
    checkRegularUser = new auth().checkUser;

module.exports = function(express) {    
    var colPath = config.baseApi + '/analytics',
        init,
        addCounter;
    
    init = function() {
        express.post(colPath + '/:ansqrId', addCounter);
    };
    
    addCounter = function addCounter(req, res) {
        var ansqrId = req.params.ansqrId,
            promise;
        
        promise = db.ansqrModel.findById(ansqrId).exec();
        
        promise.onReject(function(err) {
            res.send(500, err);
        });
        
        promise.onFulfill(function(ansqr) {
            
        });
    };    
    
    return {
        init: init
    };
};