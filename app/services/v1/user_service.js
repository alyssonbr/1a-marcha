/**
 * Description: User login and profile services
 * Author: Arlindo Neto
 * Created at: 2014-01-07
 */

var _ = require('underscore'),
    fileUtils = require('../../utils/file'),
    db = require('../../db/db'),
    env = process.env.NODE_ENV || 'development',
    config = require('../../conf/config')[env],
    auth = require('../auth'),
    emailUtils = require('../../utils/email'),
    checkRegularUser = new auth().checkUser;

module.exports = function(express) {
    var colPath = config.baseApi + '/users';

    var init = function() {
        express.get(colPath, getUsers);
        express.post(colPath + '/login', login);
        express.post(colPath + '/login-fb', loginFacebook);
        express.post(colPath + '/login-gg', loginGoogle);
        express.post(colPath, createUser);
        express.put(colPath + '/:id', checkRegularUser, updateUser);
        express.post(colPath + '/remember', rememberPassword);
    };

    var getUsers = function getUsers(req, res) {
        var skip, limit, query,
            promise;

        skip = req.query['skip'] ? req.query['skip'] : 0;
        limit = req.query['limit'] ? req.query['limit'] : 10;
        query = req.query['query'] ? req.query['query'] : '';

        promise = db.userModel.findByEmailOrName(query, skip, limit);

        promise.onReject(function(err) {
            res.send(500, err);
        });

        promise.onFulfill(function(list) {
            if(list.length == 0) {
                res.send(404);
            } else {
                res.send(200, list);
            }
        });
    };

    /*
     * Login service function
     */
    var login = function login(req, res) {
        var email = req.body.email;
        var password = req.body.password;

        /*
         * Validations
         */
        if(!email || !password) {
            res.send(400, 'No data available');

            return;
        }

        db.init();
        var userModel = db.userModel;

        var promise = userModel.findByEmail(email);
        promise.onReject(function(err) {
            res.send(500, err);
        });

        promise.onFulfill(function(user) {
            if(! user) {
                res.send(404, 'User not found');
            } else {
                var userPassword = db.userModel.createPassword(password);
                if(userPassword != user.password) {
                    res.send(403, 'Invalid password.');
                } else {
                    /*
                     * Process user token
                     */
                    user.createToken();

                    user.save(function(err, data) {
                        user.password = '';

                        res.send(200, user);
                    });
                }
            }
        });

        promise.onResolve(function() {

        });
    };

    /*
     * Login service function
     */
    var loginFacebook = function loginFacebook(req, res) {
        var email = req.body.email,
            firstName = req.body.firstName,
            lastName = req.body.lastName,
            facebookId = req.body.id,
            photoUrl = 'http://graph.facebook.com/'+facebookId+'/picture?type=large',
            saveImagePromise,
            newToken;

        /*
         * Validations
         */
        if(!email || !firstName || !lastName || !facebookId) {
            res.send(400, 'No data available');

            return;
        }

        db.init();
        var userModel = db.userModel;

        var promise = userModel.findOne({'social.facebookId': facebookId}).exec();
        promise.onReject(function(err) {
            res.send(500, err);
        });

        promise.onFulfill(function(user) {
            if(! user) {
                // create new one
                saveImagePromise = fileUtils.saveProfileImage(photoUrl, facebookId);

                saveImagePromise.onFulfill(function(fileName) {
                    user = new userModel({
                        email: email,
                        firstName: firstName,
                        lastName: lastName,
                        social: {
                            facebookId: facebookId
                        },
                        photoUrl: fileName,
                        password: userModel.createPassword('flaksdjçf')
                    });

                    user.createToken();
                    user.createFetchToken();

                    user.save(function(err, user) {
                        user.password = '';

                        res.send(200, user);
                    });
                });
            } else {
                /*
                 * Process user token
                 */
                newToken = user.createToken();
                saveImagePromise = fileUtils.saveProfileImage(photoUrl, user.social.facebookId);

                saveImagePromise.onFulfill(function(fileName) {
                    user.update({photoUrl: fileName, token: newToken}, function(err) {
                        user.password = '';

                        res.send(200, user);
                    });
                });
            }
        });

        promise.onResolve(function() {

        });
    };

    /*
     * Login service function
     */
    var loginGoogle = function loginGoogle(req, res) {
        var email = req.body.email,
            firstName = req.body.firstName,
            lastName = req.body.lastName,
            googleId = req.body.id,
            photoUrl = req.body.photo,
            saveImagePromise,
            newToken;

        /*
         * Validations
         */
        if(!email || !firstName || !googleId) {
            res.send(400, 'No data available');

            return;
        }

        /*
         * Image url processing
         */
        if(photoUrl.indexOf('?') > -1) {
            photoUrl = photoUrl.split("?")[0] + '?sz=600';
        }

        db.init();
        var userModel = db.userModel;

        var promise = userModel.findOne({'social.googleId': googleId}).exec();
        promise.onReject(function(err) {
            res.send(500, err);
        });

        promise.onFulfill(function(user) {
            if(! user) {
                // create new one
                saveImagePromise = fileUtils.saveProfileImage(photoUrl, googleId);

                saveImagePromise.onFulfill(function(fileName) {

                    user = new userModel({
                        email: email,
                        firstName: firstName,
                        lastName: lastName,
                        social: {
                            googleId: googleId
                        },
                        photoUrl: fileName,
                        password: userModel.createPassword('flaksdjçf')
                    });

                    user.createToken();
                    user.createFetchToken();

                    user.save(function(err, user) {
                        user.password = '';

                        res.send(200, user);
                    });

                });
            } else {
                /*
                 * Process user token
                 */
                newToken = user.createToken();
                saveImagePromise = fileUtils.saveProfileImage(photoUrl, user.social.googleId);

                saveImagePromise.onFulfill(function(fileName) {
                    user.update({photoUrl: fileName, token: newToken}, function(err) {
                        user.password = '';

                        res.send(200, user);
                    });
                });
            }
        });

        promise.onResolve(function() {

        });
    };

    /*
     * Create user service function
     */
    var createUser = function createUser(req, res) {
        var email = req.body.email,
        	password = req.body.password,
            promise;

        /*
         * Validations
         */
        if(!password || !email) {
            res.send(400, 'No data available');

            return;
        }

        db.init();

        promise = db.userModel.findByEmail(email);
        promise.onReject(function(err) {
            res.send(500, err);
        });

        promise.onFulfill(function(user) {
            if(! user) {
                req.body.password = db.userModel.createPassword(password);
                req.body.firstName = email;
                req.body.active = false;

                user = new db.userModel(req.body);

                /*
                 * Create tokens
                 */
                user.createToken();
                user.createFetchToken();

                user.save(function(err, user) {
                    if(err) {
                        if(err.name == "ValidationError") {
                            res.send(415, err.errors);
                        } else {
                            res.send(500, err);
                        }
                    } else {
                        user.password = '';

                        res.send(201, user);
                    }
                });
            } else {
                res.send(409, 'Email already in use.');
            }
        });

        promise.onResolve(function() {

        });
    };

    /*
     * Create user service function
     */
    var updateUser = function updateUser(req, res) {
        var userId = req.params.id,
            promise;

        db.init();
        
        if(req.user) {
            /*
                 * Adjustments
                 */
            // do not update username
            delete req.body.userName;
            // do not update the user profile
            delete req.body.profile;
            
            // workaround to version field
            delete req.body.__v;                  

            // just update password if any was informed
            if(req.body.password && req.body.password.trim() != '') {
                req.body.password = db.userModel.createPassword(req.body.password);
            } else {
                delete req.body.password;
            }

            _.extend(req.user, req.body);
            
            req.user.save(function(err, user) {
                if(err) {
                    if(err.name == "ValidationError") {
                        res.send(415, err.errors);
                    } else {
                        console.log(err);
                        res.send(500, err);
                    }
                } else {
                    user.password = '';

                    res.send(200, user); 
                }
            });
        }
    };

    var rememberPassword = function rememberPassword(req, res) {
        var promise, mailPromise, email;

        email = req.body.email;

        db.init();
        
        promise = db.userModel.findByEmail(email);
		
        promise.onReject(function(err) {
            res.send(500, err);
        });

        promise.onFulfill(function(user) {
            if(!user) {
                res.send(404, 'User not found');
            } else {
                var password = db.userModel.createPassword(user.password + Math.random().toString(36));
                user.password = db.userModel.createPassword(password);
                
                user.save(function(err) {
                    if(err) {
                        res.send(500, err);
                        
                        return;
                    }
                
                    mailPromise = emailUtils.sendEmail(user.email, 'Nova Senha', "Olá,\n\nComo requerido, uma nova senha foi enviada.\nÉ importante que, assim que fizer o próximo login, você defina uma nova no seu perfil de usuário.\n\nNova Senha: " + password);

                    mailPromise.onReject(function(err) {
                        res.send(500, err);
                    });

                    mailPromise.onFulfill(function(message) {
                        res.send(200, message);
                    });
                });
            }
        });
    };

    return {
        init: init
    };
};