var fileUtils = require('../../utils/file'),
    env = process.env.NODE_ENV || 'development',
    db = require('../../db/db'),
    config = require('../../conf/config')[env],
    auth = require('../auth');

module.exports = function(express) {
    var addImage,
        checkUser = new auth().checkUser,
        processForVehicle,
        processForUser,
        colPath = config.baseApi + '/image'
    
    init = function() {
        express.post(colPath + '/upload/:whatFor/:id', checkUser, addImage);
    };
    
    addImage = function addImage(req, res) {
        var whatFor = req.params.whatFor,
            whatForId = req.params.id,
            promise;
        
        if(!whatFor || !whatForId || ! req.files || ! req.files.image) {
            res.send(400, 'Required data needed');
            
            return;
        }

        db.init();
        
        switch(whatFor) {
            case 'vehicle':
                processForVehicle(whatForId, req, res);
                break;
            case 'user':
                processForUser(whatForId, req, res);
                break;
            default:
                res.send(409, 'No valid option for image');
        }        
    };
    
    processForVehicle = function(vehicleId, req, res) {
        var promise = db.vehicleModel.findById(vehicleId).exec();
        
        promise.onReject(function(err) {
            res.send(500, err);
        });

        promise.onFulfill(function(vehicle) {
            if(vehicle && vehicle.owner.toHexString() == req.user._id.toHexString()) {
                fileUtils.savePublicFile(req.files.image.path, 'vehicle', function(error, fileName) {
                    if(error) {
                        res.send(500, error);

                        return;
                    }

                    vehicle.photos.push(fileName);

                    vehicle.save(function(err) {
                        if(err) {
                            res.send(500, err);

                            return;
                        }

                        res.send(200, fileName);
                    });                
                });
            } else {
                res.send(403, 'Not authorized');
            }
        });
    };
    
    processForUser = function(userId, req, res) {
        var promise = db.userModel.findById(userId).exec();
        
        promise.onReject(function(err) {
            res.send(500, err);
        });

        promise.onFulfill(function(user) {
            if(user && user._id.toHexString() == req.user._id.toHexString()) {
                fileUtils.savePublicFile(req.files.image.path, 'user', function(error, fileName) {
                    if(error) {
                        res.send(500, error);

                        return;
                    }

                    user.photoUrl = '/images/profiles/' + fileName;

                    user.save(function(err) {
                        if(err) {
                            res.send(500, err);

                            return;
                        }

                        res.send(200, {path: 'images/profiles/', fileName: fileName});
                    });                
                }, userId);
            } else {
                res.send(403, 'Not authorized');
            }
        });
    };
    
    return {
        init: init
    };
}