var mongoose = require('mongoose'),
    requireDir = require('require-dir'),
    env = process.env.NODE_ENV || 'development',
    config = require('../../app/conf/config')[env],    

    schemas = requireDir('./models', {recursive: true}),
    modelName;

exports.init = function(callback) {
    if(! mongoose.connection.readyState) {
        mongoose.connect(config.db.url, {}, function(err) {
            if(err) {
                mongoose.connection.close();
                
                console.log(err);
                throw err;
            }

            if(callback) {
                callback();
            }
        });
    } else if(callback) {
        callback();
    }
};

exports.convertId = function(id) {
    return mongoose.Types.ObjectId(id);
};

exports.close = function(callback) {
    mongoose.connection.close(function() {
        if(callback) {
            callback();
        }
    });
};

exports.getDb = function() {
    return mongoose;
};

// register schemas to export
for(schema in schemas) {
    modelName = schema.split('_')[0];
    exports[modelName + 'Model'] = mongoose.model(modelName, schemas[schema]);
}