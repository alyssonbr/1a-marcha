var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var schema = new Schema({
    yellCounter: {type: Number, default: 1}
});

schema.statics.nextYell = function() {
    var query = mongoose.model('Counter', schema).
        findOneAndUpdate({yellCounter: {$gt: 0}},
        {$inc: {yellCounter: 1}},
        {new: true, upsert: true});

    return query.exec();
};

module.exports = schema;
