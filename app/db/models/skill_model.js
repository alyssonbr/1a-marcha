var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var schema = new Schema({
    description: {type: String, default: '', trim: true}
});

module.exports = schema;