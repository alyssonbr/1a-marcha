var mongoose = require('mongoose'),
    schema;

schema = new mongoose.Schema({
    owner: {type: mongoose.Schema.Types.ObjectId, ref: 'user'},
    ansqr: {type: mongoose.Schema.Types.ObjectId, ref: 'ansqr'},
    created: {type: Date, default: new Date()}
});

module.exports = schema;