module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        shell: {                                // Task
            npm_install: {                      // Target
                options: {                      // Options
                    stdout: true
                },
                command: 'npm install --no-bin-link'
            },

            jasmine: {
                options: {
                    stdout: true
                },
                command: 'jasmine-node --forceexit --captureExceptions spec'
            }
        }
    });

    // Load the plugin that provides tasks.
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-contrib-jasmine');

    // Default task(s).
    grunt.registerTask('default', ['shell:npm_install', 'shell:jasmine']);
    grunt.registerTask('test', ['shell:jasmine']);
    grunt.registerTask('npm', ['shell:npm_install']);
};
